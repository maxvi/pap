{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Database ( Database
                , Paper ( Paper )
                , title
                , authors
                , year
                , tags
                , source
                , Source ( Arxiv, DOI, ISBN )
                , getPaper
                , addPaper
                , removePaper
                , movePaper
                , adjustPaper
                , listPapers
                , canonicalize
                , initialDatabase
                , printPapersColors
                ) where

import Data.Aeson
import Data.Aeson.Encode.Pretty
import qualified Data.Map as Map
import Data.Map (Map)
import GHC.Generics
import System.Directory
import Data.List
import Data.Maybe
import Text.Printf
import System.Console.ANSI

data Paper = Paper { title   :: String
                   , authors :: [String]
                   , year    :: Int
                   , tags    :: [String]
                   , source  :: Maybe Source
                   }
                   deriving Generic
data Source = Arxiv String | DOI String | ISBN String deriving Generic

canonicalize :: Paper -> Paper
canonicalize Paper { title, authors, year, tags, source } =
    Paper { title
          -- TODO: Sort authors?
          , authors
          , year
          , tags = nub $ sort tags
          , source
          }

instance Show Source where
    show (Arxiv id)  = "ArXiV " <> id
    show (DOI doi)   = "DOI "   <> doi
    show (ISBN isbn) = "ISBN "  <> isbn
instance Show Paper where
    show Paper { title, authors, year, tags, source } =
        "Title:    " <> title                                <> "\n"
     <> "Authors:  " <> intercalate ", " authors             <> "\n"
     <> "Year:     " <> show year                            <> "\n"
     <> "Tags:     " <> intercalate ", " tags                <> "\n"
     <> "Source:   " <> fromMaybe mempty (fmap show source)
showShort :: Paper -> String
showShort Paper { title, authors, year, tags, source } =
    "'" <> title <> "'"
 <> " by " <> intercalate ", " authors <> " (" <> (show year) <> ")"
 <> mconcat (map (" @" <>) tags)

instance ToJSON Paper where
    toEncoding = genericToEncoding defaultOptions
instance FromJSON Paper
instance ToJSON Source where
    toEncoding = genericToEncoding defaultOptions
instance FromJSON Source

data Database = Database { papers :: Map String Paper
                         }
                         deriving Generic

initialDatabase :: Database
initialDatabase = Database { papers = Map.empty }

instance ToJSON Database where
    toEncoding = genericToEncoding defaultOptions
instance FromJSON Database

assertFileExists :: FilePath -> IO ()
assertFileExists file = doesFileExist file >>=
    \case False -> fail "FILE does not exist"
          True  -> pure ()

getPaper :: Database -> FilePath -> IO Paper
getPaper Database { papers } file = fromMaybe
    (fail "FILE not a part of the index")
    (pure <$> Map.lookup file papers)
addPaper :: Database -> FilePath -> Paper -> IO Database
addPaper Database { papers } file paper = assertFileExists file
    >> pure Database { papers = Map.insert file paper papers }
removePaper :: Database -> FilePath -> IO Database
removePaper Database { papers } file
    | file `Map.notMember` papers
    = fail "FILE not a part of the index"
    | otherwise
    = pure Database { papers = Map.delete file papers }
movePaper :: Database -> FilePath -> FilePath -> IO Database
movePaper Database { papers } source target =
    case Map.lookup source papers of
        Just paper -> assertFileExists target
                   >> pure Database { papers = Map.insert target paper
                                             $ Map.delete source papers }
        Nothing -> fail "SOURCE not a part of the index"
adjustPaper :: Database -> FilePath -> (Paper -> Paper) -> IO Database
adjustPaper Database { papers } file f
    | file `Map.notMember` papers
    = fail "FILE not a part of the index"
    | otherwise
    = pure Database { papers = Map.adjust f file papers }
-- TODO: Use Text or Bytestring to get better performance
listPapers :: Database -> (Paper -> Bool) -> String
listPapers Database { papers } f = intercalate "\n" $
    map (\(k, v) -> k <> ": " <> showShort v) ls
    -- TODO: Option for choosing sorting
    where ls = sortOn (year . snd) $ filter (f . snd) (Map.assocs papers)

printColors :: String -> Paper -> IO ()
printColors name Paper { title, authors, year, tags, source } = do
    setSGR [SetPaletteColor Foreground 145]
    putStr (name <> ":")
    setSGR [Reset]
    putStr " '"
    putStr title
    putStr "' by "
    setSGR [SetColor Foreground Vivid Green]
    putStr $ intercalate ", " authors
    setSGR [Reset]
    putStr $ " (" <> (show year) <> ")"
    setSGR [SetColor Foreground Vivid Red]
    putStr $ mconcat (map (" @" <>) tags)
    setSGR [Reset]
    putStrLn ""
printPapersColors :: Database -> (Paper -> Bool) -> IO ()
printPapersColors Database { papers } f = sequence_ $
    map (\(k, v) -> printColors k v) ls
    -- TODO: Option for choosing sorting
    where ls = sortOn (year . snd) $ filter (f . snd) (Map.assocs papers)
