{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Doi ( getDoiPaper, getDoiPdfUrl ) where

import Database
import Data.Aeson
import Data.Aeson.Types
import Network.HTTP.Conduit
import Network.HTTP.Simple ( addRequestHeader, getRequestHeader )
import Text.XML
import qualified Text.HTML.DOM as HTML
import qualified Data.Text as Text
import Data.Text ( Text )
import qualified Data.Map.Strict as Map
import Data.List
import Data.Maybe

firstJust :: (a -> Maybe b) -> [a] -> Maybe b
firstJust f ls = listToMaybe $ mapMaybe f ls

extractPaper :: String -> Object -> Parser Paper
extractPaper doi obj = do
    title       <- obj .: "title"
    authorField <- obj .: "author"
    authors <- sequence $
        map (withObject "author" $ \a -> do
                given  <- a .: "given"
                family <- a .: "family"
                pure $ given <> " " <> family
            )
            authorField
    publishedPrint <- obj .: "published-print"
    dateParts      <- publishedPrint .: "date-parts"
    year           <- maybeToIO (listToMaybe dateParts >>= listToMaybe)
    pure Paper { title
               , year
               , authors
               , tags = []
               , source = Just $ DOI doi
               }
    where
        doiError = (fail "bad DOI response")
        maybeToIO x = fromMaybe doiError $ pure <$> x

getDoiPaper :: String -> IO Paper
getDoiPaper doi = do
    req <- addRequestHeader
        "Accept"
        "application/rdf+xml;q=0.5, application/vnd.citationstyles.csl+json;q=1.0"
        <$> parseRequest url
    manager <- newManager tlsManagerSettings
    json <- responseBody <$> httpLbs req manager
    parsed <- either fail pure $ eitherDecode json
    let paper = withObject "res" (extractPaper doi) parsed
    either fail pure $ parseEither (const paper) ()
    where url = "http://dx.doi.org/" <> doi

getDoiPdfUrl :: String -> IO String
getDoiPdfUrl doi = do
    html <- simpleHttp ("https://sci-hub.se/https://doi.org/" <> doi)
    let doc = documentRoot $ HTML.parseLBS html

    body <- getTag "body" doc
    let articles = filter
            (\Element { elementAttributes } -> Map.lookup "id" elementAttributes == Just "article")
            (getTags "div" body)
    article <- maybeToIO (listToMaybe articles)
    iframe <- getTag "embed" article
    let Element { elementAttributes = iframeAttributes } = iframe
    maybeToIO $ Text.unpack <$> Map.lookup "src" iframeAttributes

    where
        scihubError = (fail "bad SciHub response")
        maybeToIO x = fromMaybe scihubError $ pure <$> x
        getTag tag Element { elementNodes } = maybeToIO $
            firstJust
                (\case NodeElement e @ Element { elementName }
                           | nameLocalName elementName == Text.pack tag -> Just e
                       _                                                -> Nothing)
                elementNodes
        getTags tag Element { elementNodes } =
            [ e
            | node <- elementNodes
            , NodeElement e @ Element { elementName } <- [node]
            , nameLocalName elementName == Text.pack tag
            ]
        getCont Element { elementNodes } = maybeToIO $
            firstJust
                (\case NodeContent c -> Just c
                       _             -> Nothing)
                elementNodes
        cleanTitle title = cleanTitle' $ filter (/= '\n') title
        cleanTitle' (' ':' ':x) = ' ':cleanTitle' x
        cleanTitle' (x:xs)      = x:cleanTitle' xs
        cleanTitle' ""          = ""

