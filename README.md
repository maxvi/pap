# `pap`: download, tag, and index your scientific papers

`pap` is a tool for organizing a large collection of PDFs. See `pap --help` for usage.

## Integration with `fzf`

`pap` is particularly convenient when used in combination with `fzf`. For example, using the command

```
pap ls | fzf --ansi | sed 's/\x1b\[[0-9;]*m//g' | sed -n 's/^\([^:]\+\):.*$/\1/p' | { xargs -r zathura & }
```

replacing `zathura` with the PDF reader you prefer, you get a prompt like the following:

![screenshot](screenshot.png)
